# Open AF Payload Method Changelog

v3.0.1 (2023/10/13)
- Moved 'Verify AF Msg' to the llb (was previously in the wrong location).
- Made Affected Items typedef match other controls (for reference only).

v3.0.0 (2023/08/16)
- Added support for going to a payload method from a Msg Class BD constant.

v2.0.0 (2022/02/22)
- Added QuickDrop support (default Ctrl+E)

v1.0.1 (2021/11/09)
- Removed palettes since they are unnecessary

v1.0.0 (2021/11/01)
- Official public release