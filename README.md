# Open AF Payload Method

Adds a right-click and QuickDrop shortcut (default Ctrl+E) to AF Msg "Send" methods and Msg Class constants on the block diagram allowing the developer to quickly open that Msg's payload method's block diagram.